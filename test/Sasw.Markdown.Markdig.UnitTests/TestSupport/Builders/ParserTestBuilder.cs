namespace Sasw.Markdown.Markdig.UnitTests.TestSupport.Builders
{
    public static class ParserTestBuilder
    {
        public static IParser New => new Parser();
    }
}
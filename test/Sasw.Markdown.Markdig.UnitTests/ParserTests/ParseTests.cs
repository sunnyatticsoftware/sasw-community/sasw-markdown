using FluentAssertions;
using Sasw.Markdown.Markdig.UnitTests.TestSupport;
using Sasw.Markdown.Markdig.UnitTests.TestSupport.Builders;
using Xunit;

namespace Sasw.Markdown.Markdig.UnitTests.ParserTests
{
    public static class ParseTests
    {
        public class Given_A_Markdown_Paragraph_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is a paragraph";
                _sut = ParserTestBuilder.New;
                _expectedResult = $"<p>{_markdown}</p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Paragraph()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_Paragraph_With_FrontMatter_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "---\n" +
                            "foo: this is\n" +
                            "bar: ignored\n" +
                            "---\n" +
                            "This is a paragraph";
                _sut = ParserTestBuilder.New;
                _expectedResult = $"<p>This is a paragraph</p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Paragraph_Ignoring_FrontMatter()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }

        public class Given_Markdown_Paragraph_With_Inline_FrontMatter_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "---title: whatever\n" +
                            "foo: it is ignored---\n" +
                            "This is a paragraph";
                _sut = ParserTestBuilder.New;
                _expectedResult = $"<p>{_markdown}</p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_With_Front_Matter_Inside_Paragraph()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }
        
        public class Given_Markdown_Paragraph_With_Custom_Id_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is a paragraph {#custom-paragraph-id}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p id=\"custom-paragraph-id\">This is a paragraph </p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }

        public class Given_Markdown_Paragraph_With_CustomClass_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is a paragraph {.custom-paragraph-class}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p class=\"custom-paragraph-class\">This is a paragraph </p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Paragraph_With_Custom_Class()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Header_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "# This is an h1";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h1 id=\"this-is-an-h1\">This is an h1</h1>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Header_With_Autogenerated_Id()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_Header_With_Custom_Id_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "# This is an h1 {#custom-id}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h1 id=\"custom-id\">This is an h1</h1>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Header_With_Custom_Id()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Header_With_Custom_Class_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "# This is an h1 {.custom-class}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h1 id=\"this-is-an-h1\" class=\"custom-class\">This is an h1</h1>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Header_With_Custom_Class()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_SubHeader_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "## This is an h2";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h2 id=\"this-is-an-h2\">This is an h2</h2>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Subheader_With_Autogenerated_Id()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_SubHeader_With_Custom_Id_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "## This is an h2 {#custom-id}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h2 id=\"custom-id\">This is an h2</h2>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Subheader_With_Custom_Id()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_SubHeader_With_Custom_Class_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "## This is an h2 {.custom-class}";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<h2 id=\"this-is-an-h2\" class=\"custom-class\">This is an h2</h2>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Subheader_With_Custom_Class()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_Line_Code_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "`This is code` in line";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p><code>This is code</code> in line</p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Code()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Block_Code_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "~~~\n" +
                            "This is code\n" +
                            "~~~\n" +
                            "in block";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<pre><code>This is code\n</code></pre>\n<p>in block</p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Block_Code()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_Unordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "- first\n" +
                            "- second\n" +
                            "- third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ul>\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ul>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Unordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Unordered_Bullet_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "* first\n" +
                            "* second\n" +
                            "* third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ul>\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ul>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Lowercase_Alpha_Ordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "a. first\n" +
                            "b. second\n" +
                            "c. third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ol type=\"a\">\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ol>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Uppercase_Alpha_Ordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "A. first\n" +
                            "B. second\n" +
                            "C. third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ol type=\"A\">\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ol>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Lowercase_Roman_Ordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "i. first\n" +
                            "ii. second\n" +
                            "iii. third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ol type=\"i\">\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ol>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_Uppercase_Roman_Ordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "I. first\n" +
                            "II. second\n" +
                            "III. third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ol type=\"I\">\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ol>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_Markdown_With_Number_Ordered_List_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "1. first\n" +
                            "2. second\n" +
                            "3. third";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<ol>\n<li>first</li>\n<li>second</li>\n<li>third</li>\n</ol>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Ordered_List()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Bold_Characters_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is **bold**";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p>This is <strong>bold</strong></p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Bold()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Emphasis_Characters_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is *emphasis*";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p>This is <em>emphasis</em></p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Emphasis()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Underscore_Emphasis_Characters_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                _markdown = "This is _italic_";
                _sut = ParserTestBuilder.New;
                _expectedResult = "<p>This is <em>italic</em></p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Italic()
            {
                _result.Should().Be(_expectedResult);
            }
        }
        
        public class Given_Markdown_With_Image_Base64_When_Parsing
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private string _expectedResult = null!;
            private string _result = null!;

            protected override void Given()
            {
                const string imageData = "data:image/svg+xml;base64,WHATEVER";
                _markdown = $"![ImagePath Alt]({imageData})";
                _sut = ParserTestBuilder.New;
                _expectedResult = $"<p><img src=\"{imageData}\" alt=\"ImagePath Alt\" /></p>";
            }

            protected override void When()
            {
                _result = _sut.Parse(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Embedded_Image()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
            
            public class Given_Markdown_With_Image_When_Parsing
                : Given_When_Then_Test
            {
                private IParser _sut = null!;
                private string _markdown = null!;
                private string _expectedResult = null!;
                private string _result = null!;

                protected override void Given()
                {
                    const string image = "sample.png";
                    _markdown = $"![ImagePath Alt]({image})";
                    _sut = ParserTestBuilder.New;
                    _expectedResult = $"<p><img src=\"{image}\" alt=\"ImagePath Alt\" /></p>";
                }

                protected override void When()
                {
                    _result = _sut.Parse(_markdown);
                }

                [Fact]
                public void Then_It_Should_Return_The_Expected_Html_Relative_Url()
                {
                    _result.Should().BeEquivalentTo(_expectedResult);
                }
            }
            
            public class Given_Markdown_With_Absolute_Image_When_Parsing
                : Given_When_Then_Test
            {
                private IParser _sut = null!;
                private string _markdown = null!;
                private string _expectedResult = null!;
                private string _result = null!;

                protected override void Given()
                {
                    const string imageUrl = "https://foo/sample.png";
                    _markdown = $"![ImagePath Alt]({imageUrl})";
                    _sut = ParserTestBuilder.New;
                    _expectedResult = $"<p><img src=\"{imageUrl}\" alt=\"ImagePath Alt\" /></p>";
                }

                protected override void When()
                {
                    _result = _sut.Parse(_markdown);
                }

                [Fact]
                public void Then_It_Should_Return_The_Expected_Html_Absolute_Url()
                {
                    _result.Should().BeEquivalentTo(_expectedResult);
                }
            }

            public class Given_Markdown_With_Absolute_Link_When_Parsing
                : Given_When_Then_Test
            {
                private IParser _sut = null!;
                private string _markdown = null!;
                private string _expectedResult = null!;
                private string _result = null!;

                protected override void Given()
                {
                    const string url = "https://foo";
                    _markdown = $"[link]({url})";
                    _sut = ParserTestBuilder.New;
                    _expectedResult = $"<p><a href=\"{url}\">link</a></p>";
                }

                protected override void When()
                {
                    _result = _sut.Parse(_markdown);
                }

                [Fact]
                public void Then_It_Should_Return_The_Expected_Html_Absolute_Link()
                {
                    _result.Should().BeEquivalentTo(_expectedResult);
                }
            }
            
            public class Given_Markdown_With_Relative_Link_When_Parsing
                : Given_When_Then_Test
            {
                private IParser _sut = null!;
                private string _markdown = null!;
                private string _expectedResult = null!;
                private string _result = null!;

                protected override void Given()
                {
                    const string url = "foo/bar";
                    _markdown = $"[link]({url})";
                    _sut = ParserTestBuilder.New;
                    _expectedResult = $"<p><a href=\"{url}\">link</a></p>";
                }

                protected override void When()
                {
                    _result = _sut.Parse(_markdown);
                }

                [Fact]
                public void Then_It_Should_Return_The_Expected_Html_Relative_Link()
                {
                    _result.Should().BeEquivalentTo(_expectedResult);
                }
            }
            
            public class Given_Markdown_With_Latex_Math_When_Parsing
                : Given_When_Then_Test
            {
                private IParser _sut = null!;
                private string _markdown = null!;
                private string _expectedResult = null!;
                private string _result = null!;

                protected override void Given()
                {
                    _markdown = "When $ a \\ne 0 $, there are two solutions to \\(ax^2 + bx + c = 0\\) and they are $$x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}.$$";
                    _sut = ParserTestBuilder.New;
                    _expectedResult = "<p>When <span class=\"math\">\\(a \\ne 0\\)</span>, there are two solutions to (ax^2 + bx + c = 0) and they are <span class=\"math\">\\(x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}.\\)</span></p>";
                }

                protected override void When()
                {
                    _result = _sut.Parse(_markdown);
                }

                [Fact]
                public void Then_It_Should_Return_The_Expected_Html_Relative_Link()
                {
                    _result.Should().BeEquivalentTo(_expectedResult);
                }
            }
        }
    }
}
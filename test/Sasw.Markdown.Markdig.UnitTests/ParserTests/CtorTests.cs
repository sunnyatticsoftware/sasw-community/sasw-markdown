using FluentAssertions;
using Sasw.Markdown.Markdig.UnitTests.TestSupport;
using Xunit;

namespace Sasw.Markdown.Markdig.UnitTests.ParserTests
{
    public static class CtorTests
    {
        public class Given_No_Dependencies_When_Instantiating
            : Given_When_Then_Test
        {
            private Parser _sut = null!;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new Parser();
            }

            [Fact]
            public void Then_It_Should_Instantiate()
            {
                _sut.Should().NotBeNull();
            }
            
            [Fact]
            public void Then_It_Should_Be_An_IParser()
            {
                _sut.Should().BeAssignableTo<IParser>();
            }
        }
    }
}
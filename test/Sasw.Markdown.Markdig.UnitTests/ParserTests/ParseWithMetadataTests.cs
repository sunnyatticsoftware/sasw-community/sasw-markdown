using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Sasw.Markdown.Markdig.UnitTests.TestSupport;
using Sasw.Markdown.Markdig.UnitTests.TestSupport.Builders;
using Xunit;

namespace Sasw.Markdown.Markdig.UnitTests.ParserTests
{
    public static class ParseWithMetadataTests
    {
        public class Given_A_Markdown_With_Front_Matter_When_Parsing_With_Metadata
            : Given_When_Then_Test
        {
            private IParser _sut = null!;
            private string _markdown = null!;
            private ParseResult<Metadata> _expectedResult = null!;
            private ParseResult<Metadata> _result = null!;

            class Metadata
            {
                public string Single { get; init; } = string.Empty;
                public IEnumerable<string> Multiple { get; init; } = Enumerable.Empty<string>();
                public DateTime Date { get; init; }
                public bool Flag { get; init; }
                public Nested Nested { get; init; } = new();
                public IEnumerable<Nested> MultipleNested { get; set; }
            }

            class Nested
            {
                public int Number { get; init; }
                public decimal Decimal { get; init; }
            }
            
            protected override void Given()
            {
                _markdown = "---\n" +
                            "single: one\n" +
                            "multiple:\n" +
                            "   - one\n" +
                            "   - Two\n" +
                            "   - THREE\n" +
                            "date: 2021-12-01\n" +
                            "flag: true\n" +
                            "nested:\n" +
                            "   number: 123\n" +
                            "   decimal: 12.3\n" +
                            "multipleNested:\n" +
                            "   - number: 456\n" +
                            "     decimal: 45.6\n" +
                            "   - number: 789\n" +
                            "     decimal: 78.9\n" +
                            "---\n" +
                            "This is a paragraph with **bold characters**";
                _sut = ParserTestBuilder.New;
                _expectedResult =
                    new ParseResult<Metadata>
                    {
                        Html = "<p>This is a paragraph with <strong>bold characters</strong></p>",
                        Metadata =
                            new Metadata
                            {
                                Single = "one",
                                Multiple = new List<string> {"one", "Two", "THREE"},
                                Date = new DateTime(2021, 12, 1),
                                Flag = true,
                                Nested = 
                                    new Nested
                                    {
                                        Number = 123,
                                        Decimal = 12.3m
                                    },
                                MultipleNested = 
                                    new List<Nested>
                                    {
                                        new Nested
                                        {
                                            Number = 456,
                                            Decimal = 45.6m
                                        },
                                        new Nested
                                        {
                                            Number = 789,
                                            Decimal = 78.9m
                                        }
                                    }
                            }
                    };
            }

            protected override void When()
            {
                _result = _sut.ParseWithMetadata<Metadata>(_markdown);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Html_Paragraph()
            {
                _result.Should().BeEquivalentTo(_expectedResult);
            }
        }
    }
}
﻿using System;
using System.Linq;
using Markdig;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Sasw.Markdown.Markdig
{
    public sealed class Parser
        : IParser
    {
        private readonly MarkdownPipeline _pipeline;

        public Parser()
        {
            _pipeline =
                new MarkdownPipelineBuilder()
                    .UseAdvancedExtensions()
                    .UseYamlFrontMatter()
                    .UseMathematics()
                    .Build();
        }
        
        public string Parse(string markdown)
        {
            var html = global::Markdig.Markdown.ToHtml(markdown, _pipeline).Trim();
            return html;
        }
        
        public ParseResult<T> ParseWithMetadata<T>(string markdown) where T : class, new()
        {
            var html = Parse(markdown);
            
            var yamlDeserializer =
                new DeserializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .IgnoreUnmatchedProperties()
                    .Build();
            
            const string frontMatterDelimiter = "---";
            var sections = markdown.Split(frontMatterDelimiter);
            var frontMatter = sections.ElementAtOrDefault(1)?.TrimStart('\n', '\r');

            try
            {
                var metadata =
                    frontMatter is not null
                        ? yamlDeserializer.Deserialize<T>(frontMatter)
                        : new T();

                var result =
                    new ParseResult<T>
                    {
                        Html = html,
                        Metadata = metadata
                    };
                return result;
            }
            catch (Exception exception)
            {
                return new ParseResult<T>{Html = html};
            }
        }
    }
}

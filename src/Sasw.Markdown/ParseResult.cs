namespace Sasw.Markdown
{
    public sealed class ParseResult<T>
        where T: class, new()
    {
        public string Html { get; init; } = string.Empty;
        public T Metadata { get; init; } = new T();
    }
}
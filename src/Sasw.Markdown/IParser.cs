﻿namespace Sasw.Markdown
{
    public interface IParser
    {
        /// <summary>
        /// Parses markdown text. It ignores YAML front matter.
        /// </summary>
        /// <param name="markdown">the input markdown</param>
        /// <returns>the html</returns>
        string Parse(string markdown);
        
        /// <summary>
        /// Parses markdown text and, if exists, it also parses YAML front matter into given type
        /// </summary>
        /// <param name="markdown">the input markdown with optional front matter YAML</param>
        /// <typeparam name="T">the destination type for front matter deserialization</typeparam>
        /// <returns>the parse result with the html and the front matter metadata</returns>
        ParseResult<T> ParseWithMetadata<T>(string markdown) where T: class, new();
    }
}

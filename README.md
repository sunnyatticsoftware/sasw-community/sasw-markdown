# sasw-markdown
Library to parse markdown with YAML front matteer and formula Latex, and produce HTML

## How to use
There is a contract `IParser` with an operation to parse markdown to Html or markdown + front matter to Html, for input that contains metadata.

The specific implementation uses `Markdig` and `YamlDotNet`. Below there is a sample on how to use.

For a given markdown with metadata such as `sample.md`
```
---
title: This is my title
published: 2021-12-01
author:
    id: 123
    name: Joe Bloggs
category: articles
tags:
    - news
    - opinion
    - technology
---
This is a sample text capable of _all_ markdown **features**:
1. Markdown
2. Front Matter in YAML
3. For more info see [this](https://github.com/xoofx/markdig)
```

it can be rendered into HTML with

```
class MyMetadata
{
    public string Title { get; init; } = string.Empty;
    public DateTime Published { get; init; }
    public string Category { get; init; } = string.Empty;
    public MyAuthor Author { get; init; } = new();
    public IEnumerable<string> Tags { get; init; } = Enumerable.Empty<string>();
}

class MyAuthor
{
    public int Id { get; init; }
    public string Name { get; init; } = string.Empty;
}

var markdown = FakeFileUtil.GetAsString("sample.md"); // Assume the markdown input is a string
IParser parser = new Parser();
ParseResult<MyMetadata> result = parser.ParseWithMetadata<MyMetadata>(markdown);
```

For more details view the unit tests.